<?php
/**
 * Тестовое задание для "Губернских аптек" - часть 2 (PHP)
 * PHP Version 8.3.2
 * Иван Шкуратов, 2024
 */

ini_set('error_reporting', E_ALL);
ini_set('display_errors', E_ALL);

require_once 'functions.php';

// Можно переопределить значения аргументов функций (NB: в соотв. с типами переменных!)

/*
 1. Напишите программу на PHP, которая принимает на вход два числа, складывает их и выводит результат на экран.
*/
$firstNumber = 2.5; // int|float
$secondNumber = -1; // int|float

echo '<p>Сумма ' . $firstNumber . ' и ' . $secondNumber . ' равняется ';
echoSum($firstNumber, $secondNumber);
echo '</p>';
echo '<hr>';

/*
 2. Напишите функцию на PHP, которая определяет, является ли заданное число простым.
*/
$number = 4786868687777777; // int

if (isPrime($number)) {
    echo "<p>$number - это простое число</p>";
} else {
    echo "<p>$number - не является простым числом</p>";
}
echo '<hr>';

/*
 3. Напишите программу на PHP, которая принимает на вход строку и выводит ее на экран в обратном порядке.
 */
$str = 'Перевернутая строка'; // string

echo "<p>Строка '$str' в обратном порядке: '";
echoRevStr($str);
echo "'</p>";
echo '<hr>';

/*
 4. Напишите функцию на PHP, которая принимает на вход массив чисел и возвращает среднее арифметическое его элементов
 */
$array = [0, 0, 2, -3, 5.5]; // array(int|float)

echo '<p>Среднее арифметическое суммы чисел в массиве<br>';
print_r($array);
echo '</br>' . getAverage($array) . '</p>';
echo '<hr>';

/*
 5. Напишите программу на PHP, которая принимает на вход два массива чисел и выводит на экран элементы, которые встречаются в обоих массивах.
 */
$firstArray = [1, 3, 2, -1.5, 157, 2000]; // array(int|float)
$secondArray = [157, 3, -1.5, 2024]; // array(int|float)

echo '<p>Первый массив:<br>';
print_r($firstArray);
echo '<br>Второй массив:<br>';
print_r($secondArray);
echo '<br>Общие элементы двух массивов:<br>';
printIntersect($firstArray, $secondArray);
echo '</p>';
echo '<hr>';

/*
 6. Напишите функцию на PHP, которая находит максимальный элемент в массиве чисел.
 */
$numbers = [0, -1, 3, 4, '5', 5.5]; // array(int|float)

echo '<p>Максимальный элемент в массиве<br>';
print_r($numbers);
echo '<br>';
echo getMaxElement($numbers);
echo '</p>';
echo '<hr>';

/*
 7. Напишите программу на PHP, которая принимает на вход строку и проверяет, является ли она палиндромом (читается одинаково справа налево и слева направо).
 */
$someStr = 'jhg2ёёффёё2ghj'; // string

if (isPalindrome($someStr)) {
    echo "<p>Строка '$someStr' является палиндромом</p>";
} else {
    echo "<p>Строка '$someStr' не является палиндромом</p>";
}
echo '<hr>';

/*
 8. Напишите функцию на PHP, которая проверяет, является ли заданная строка валидным email адресом.
 - Насколько я знаю, серебрянной пули пока не изобрели...
 */
$email = 'name.surname@test-email.com'; // string

if (isValidEmail($email)) {
    echo "<p>Строка '$email' является валидным email адресом</p>";
} else {
    echo "<p>Строка '$email' не является валидным email адресом</p>";
}
echo '<hr>';

/*
 9. Напишите программу на PHP, которая выводит на экран таблицу умножения от 1 до 10.
 */
echo '<p>Таблица умножения</p>';
printTimesTables();
echo '<hr>';

/*
 10. Напишите функцию на PHP, которая принимает на вход массив чисел и возвращает
отсортированный по возрастанию массив
 */
$sourceArr = [4, 3, '5', 0, -1, 5.5]; // array(int|float)

echo '<p>Исходный массив чисел:<br>';
print_r($sourceArr);
echo '<br>Отсортированный по возрастанию массив:<br>';
print_r(getSortArr($sourceArr));
echo '</p>';
