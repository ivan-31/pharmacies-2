<?php
/**
 * Тестовое задание для "Губернских аптек" - часть 2 (PHP)
 * Функции для решения задач, определенных в index.php
 * Иван Шкуратов, 2024
 */

/**
 * Выводит на экран сумму чисел
 * @param float|int $firstNumber, $secondNumber
 */
function echoSum(float|int $firstNumber, float|int $secondNumber) : void {
    echo $firstNumber + $secondNumber;
}

/**
 * Проверяет, является ли переданное число простым
 * @param int $number
 * @return bool
 */
function isPrime(int $number) : bool {
    if ($number <= 1) return false;
    $sqrtNumber = sqrt($number);
    for ($i = 2; $i <= $sqrtNumber; $i++) {
        if ($number % $i == 0) {
            return false;
        }
    }
    return true;
}

/**
 * Вспомогательная функция, которая возвращает строку в обратном порядке (Unicode)
 * @param string $str
 * @return string
 */
function revStr(string $str) : string {
    preg_match_all('/./us', $str, $arr);
    return implode(array_reverse($arr[0]));
}

/**
 * Выводит на экран строку в обратном порядке
 * @param string $str
 */
function echoRevStr(string $str) : void {
    echo revStr($str);
}

/**
 * Возвращает среднее арифметическое в массиве
 * @param array $array
 * @return int|float
 */
function getAverage(array $array) : int|float {
    if (array_filter($array)) {
        return array_sum($array) / count($array);
    }
    return 0; // 0/n
}

/**
 * Выводит на экран общие элементы двух массивов
 * @param array $arr1
 * @param array $arr2
 */
function printIntersect(array $arr1, array $arr2) : void {
    print_r(array_intersect($arr1, $arr2));
}

/**
 * Найти макс. элемент в массиве чисел
 * @param array $arr
 * @return int
 */
function getMaxElement(array $arr) : int|float {
    return max($arr);
}

/**
 * Проверяет, является ли строка палиндромом
 * @param string $str
 * @return bool
 */
function isPalindrome(string $str) : bool {
    return revStr($str) == $str;
}

/**
 * Проверяет, является ли строка валидным email адресом
 * @param string $email
 * @return bool
 */
function isValidEmail(string $email) : bool {
    return (bool) filter_var($email, FILTER_VALIDATE_EMAIL);
}

/**
 * Выводит на экран таблицу умножения
 */
function printTimesTables() : void {
    $rows = 10;
    $cols = 10;

    for ($tr = 1; $tr <= $rows; $tr++) {
        echo "<table border='1' width='300px'>";
        echo "<tr>";
        for ($td = 1; $td <= $cols; $td++) {
            echo "<td width='30px'>" . $tr * $td . "</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
}

/**
 * Возвращает отсортированный по возрастанию массив чисел
 * @param array $arr
 * @return array
 */
function getSortArr(array $arr) : array {
    sort($arr, SORT_NUMERIC);
    return $arr;
}